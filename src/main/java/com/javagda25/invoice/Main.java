package com.javagda25.invoice;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Main {
    private final static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        EntityDao dao = new EntityDao();

        Invoice i = new Invoice();
        i.setClientName("ja");
        dao.saveOrUpdate(i);


        Invoice h = new Invoice();
        h.setClientName("ty");
        dao.saveOrUpdate(h);

        Product p = new Product();
        p.setInvoice(i);
        p.setName("costam");
        p.setPrice(1.0);
        p.setStock(5);
        p.setTax(1.1);
        dao.saveOrUpdate(p);

        Product c = new Product();
        c.setInvoice(i);
        c.setName("costam");
        c.setPrice(1.0);
        c.setStock(5);
        c.setTax(1.1);
        dao.saveOrUpdate(c);


        InvoiceDao invoiceDao = new InvoiceDao();
        String komenda;
        do {
            komenda = scanner.nextLine();

            if (komenda.equalsIgnoreCase("1")) {
                String jakiinvoice = scanner.nextLine();
                List<Product> products = invoiceDao.getAllProductsFromInvoice2(Long.parseLong(jakiinvoice));
                System.out.println(products);
            } else if (komenda.equalsIgnoreCase("2")) {
                String jakiinvoice = scanner.nextLine();
                invoiceDao.getSum(Long.parseLong(jakiinvoice));
            }

        } while (!komenda.equalsIgnoreCase("quit"));

    }
}

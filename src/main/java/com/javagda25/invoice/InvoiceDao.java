package com.javagda25.invoice;

import org.hibernate.Session;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InvoiceDao {
    private EntityDao entityDao = new EntityDao();

    public List<Invoice> getAllUnpaid() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();

            CriteriaQuery<Invoice> query = cb.createQuery(Invoice.class);

            Root<Invoice> root = query.from(Invoice.class);

            query.select(root).where(
                    cb.equal(root.get("ifPaid"), 0));

            return session.createQuery(query).getResultList();
        }
    }

    public List<Invoice> getAllFromLastWeek() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();

            CriteriaQuery<Invoice> query = cb.createQuery(Invoice.class);

            Root<Invoice> root = query.from(Invoice.class);

            query.select(root).where(
                    cb.between(
                            root.get("dateOfCreation"),
                            LocalDateTime.now().minusDays(7),
                            LocalDateTime.now()
                    )
            ).orderBy(cb.asc(root.get("dateOfCreation")));

            return session.createQuery(query).setMaxResults(1).getResultList();
        }
    }


    public void getSum(Long id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();

            CriteriaQuery<Double> query = cb.createQuery(Double.class);

            Root<Invoice> root = query.from(Invoice.class);
            Join<Invoice, Product> join = root.join("product", JoinType.LEFT);

            query.select(
                    cb.sum(
                            cb.prod(
                                    join.get("price"),
                                    join.get("stock")
                            )
                    )
            ).where(
                    cb.between(
                            root.get("dateOfCreation"),
                            LocalDate.now().atStartOfDay(),
                            LocalDateTime.now()
                    )
            );

            System.out.println(session.createQuery(query).getResultList());
        }
    }

    public List<Product> getAllProductsFromInvoice(Long id) {
        List<Product> productList = new ArrayList<>();

        Optional<Invoice> invoiceOptional = entityDao.getById(Invoice.class, id);
        if (invoiceOptional.isPresent()) {
            productList.addAll(invoiceOptional.get().getProduct());
        }
        return productList;
    }

    public List<Product> getAllProductsFromInvoice2(Long id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();

            CriteriaQuery<Product> query = cb.createQuery(Product.class);

            Root<Product> root = query.from(Product.class);

            query.select(root).where(
                    cb.equal(
                            root.get("invoice"),
                            id
                    )
            );

            return session.createQuery(query).getResultList();
        }
    }

}
